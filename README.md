# accurate_achdi

A new Flutter project.


The project for Technical Test Accurate

Goals & Nice to have :

A. How to use <br/>
    1. show user list  <br/>
        - go to home application <br/>
    2. find user <br/>
        - filter with search bar <br/>
    3. Sorting list by name <br/>
        - sorting list by name with icon sorting in right corner toolbar <br/>
    4. Filter list by city <br/>
        - filter list with icon filter in toolbar and you can filter with multiple city <br/>
    5. Add new user <br/>
        - add new user with icon button plus (+) on bottom screen  <br/>

B. Use technology : <br/>
    1. [Get](https://pub.dev/packages/get) <br/>
    2. [Form Builder](https://pub.dev/packages/flutter_form_builder) <br/>
    3. [Dio](https://pub.dev/packages/dio) <br/>
    4. [Logger](https://pub.dev/packages/logger) <br/>
    5. [JSONAnnotation](https://pub.dev/packages/json_annotation) <br/>
    6. [FlutterToast](https://pub.dev/packages/fluttertoast) <br/>

C. Reason UI/UX <br/>
    1. easy to use <br/>
    2. easy to implementation <br/>
    3. make it simple <br/>

import 'package:get/get.dart';
import 'package:logger/logger.dart';

class Log {
  static const String tag = 'ACHDI-LOG';
  static int maxLen = 128;
  static String tagValue = tag;

  late Logger _logger;

  Log._internal() {
    _logger = Logger(
      printer: PrettyPrinter(
        methodCount: 0,
        errorMethodCount: 5,
        lineLength: 50,
        colors: true,
        printEmojis: true,
        printTime: false,
        noBoxingByDefault: true,
      ),
    );
  }

  static Logger to = _instance._logger;

  factory Log() => _instance;

  static final Log _instance = Log._internal();

  static void verbose(dynamic message) {
    if (Get.isLogEnable) {
      to.v(message);
    }
  }

  static void debug(dynamic message) {
    if (Get.isLogEnable) {
      to.d(message);
    }
  }

  static void info(dynamic message) {
    if (Get.isLogEnable) {
      to.i(message);
    }
  }

  static void warning(dynamic message) {
    if (Get.isLogEnable) {
      to.w(message);
    }
  }

  static void error(dynamic message) {
    if (Get.isLogEnable) {
      to.e(message);
    } else {
      // ignore: todo
      // TODO: send to crash analytic
    }
  }

  static void wtf(dynamic message) {
    if (Get.isLogEnable) {
      to.wtf(message);
    } else {
      // ignore: todo
      // TODO: send to crash analytic
    }
  }

  static void write(String text, {bool isError = false}) {
    if (Get.isLogEnable) {
      if (!isError) {
        info('[ACHDI] $text');
      } else {
        error('[ACHDI] $text');
      }
    }
  }
}

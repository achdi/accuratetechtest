import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import 'pretty_log.dart';

class DioClient {
  // final Dio _dio = Dio();
  static final DioClient _instance = DioClient._internal();
  factory DioClient() => _instance;
  static bool get isDebug => kDebugMode;

  Dio? _dio;

  final _baseUrl = 'https://627e360ab75a25d3f3b37d5a.mockapi.io/api/v1';

  DioClient._internal() {
    BaseOptions options = BaseOptions(
      baseUrl: _baseUrl,
      headers: {},
      contentType: 'application/json; charset=utf-8',
      responseType: ResponseType.json,
    );

    _dio = Dio(options);
    _dio!.interceptors.clear();

    if (isDebug) {
      _dio!.interceptors.add(HttpPrettyLogger());
    }
  }

  Future<T> post<T>(String url, dynamic data, {Options? options}) async {
    try {
      Response response = await _dio!.post(
        url,
        data: data,
        options: _checkOptions(Method.get.value, options),
      );
      return response.data;
    } catch (e) {
      return Future.error(e);
    }
  }

  Future<T> get<T>(String url, {Options? options}) async {
    try {
      Response response = await _dio!.get(
        url,
        options: _checkOptions(Method.get.value, options),
      );
      return response.data;
    } catch (e) {
      return Future.error(e);
    }
  }

  Options _checkOptions(String method, Options? options) {
    options ??= Options();
    options.method = method;
    return options;
  }
}

Map<String, dynamic> parseData(String data) {
  return json.decode(data) as Map<String, dynamic>;
}

enum Method { get, post, put, patch, delete, head }

extension MethodExtension on Method {
  String get value => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD'][index];
}
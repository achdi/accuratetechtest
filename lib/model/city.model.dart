import 'package:json_annotation/json_annotation.dart';

import '../log.utils.dart';

part 'city.model.g.dart';

@JsonSerializable()
class CityModel {
  CityModel({
    required this.id,
    required this.name,
  });
  
  String id;
  String name;

  factory CityModel.fromJson(Map<String, dynamic> json) => _$CityModelFromJson(json);

  Map<String, dynamic> toJson() => _$CityModelToJson(this);

  static List<CityModel> fromJsonArray(json) {
    var users = List<CityModel>.empty(growable: true);
    if (json != null) {
      json.forEach((v) {
        users.add(CityModel.fromJson(v)); // ini jangan lupa di sesuaikan extend
      });
    }
    return users;
  }
}
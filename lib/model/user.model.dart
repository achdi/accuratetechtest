import 'package:accurate_achdi/log.utils.dart';
import 'package:json_annotation/json_annotation.dart';
part 'user.model.g.dart';

@JsonSerializable()
class UserModel {
  UserModel({
    required this.id,
    required this.email,
    required this.name,
    required this.phoneNumber,
    required this.city,
    required this.address,
  });
  
  String id;
  String email;
  String name;
  String phoneNumber;
  String city;
  String address;

  factory UserModel.fromJson(Map<String, dynamic> json) => _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);

  static List<UserModel> fromJsonArray(json) {
    var users = List<UserModel>.empty(growable: true);
    if (json != null) {
      Log.debug("fromJsonArray");
      json.forEach((v) {
        users.add(UserModel.fromJson(v)); // ini jangan lupa di sesuaikan extend
      });
    }
    return users;
  }
}
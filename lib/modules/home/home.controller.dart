import 'package:accurate_achdi/log.utils.dart';
import 'package:accurate_achdi/modules/home/home.api.dart';
import 'package:accurate_achdi/modules/home/home.state.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  final state = HomeState();
  TextEditingController searchController = TextEditingController();

  @override
  Future<void> onInit() async {
    super.onInit();
    await getData();
  }


  Future<void> getData() async {
    Future.wait([HomeApi.getUser(), HomeApi.getCities()]).then((value) {
      Log.debug("getData group");
      Log.debug(value);
      
      state.users = value[0];
      state.usersUnfiltered = value[0];
      state.users.sort(((a, b) => a.name.toLowerCase().trimLeft().trimRight().compareTo(b.name.toLowerCase().trimLeft().trimRight())));
      state.usersUnfiltered.sort(((a, b) => a.name.toLowerCase().trimLeft().trimRight().compareTo(b.name.toLowerCase().trimLeft().trimRight())));
      Log.debug("HomeController success set list");
      state.cities = value[1];
      Log.debug("HomeController success set list cities");
      state.isLoading = false;
    }).onError((error, stackTrace) {
      state.isLoading = false;
    });

    // await HomeApi.getUser()
    //   .then((response) {
    //     Log.debug("HomeController");
    //     state.users = response;
    //     usersUnfiltered = response;
    //     Log.debug("HomeController success set list");
    //     state.isLoading = false;
    //   })
    //   .onError((error, stackTrace) {
    //     Log.debug("HomeController error");
    //     Log.debug(error);
    //     state.isLoading = false;
    //   });
  }

  Future<void> filterUser() async {
    switch (searchController.text.length) {
      case 0:
        Log.debug("search empty");
        if (state.citiesSelected.isNotEmpty) {
          var newUsers = state.usersUnfiltered.where((user) {
            var citySelected = state.citiesSelected.firstWhereOrNull((element) => element.toLowerCase() == user.city.toLowerCase());
            return citySelected != null;
          }).toList();
          state.users = newUsers;
        } else {
          state.users = state.usersUnfiltered;
        }
        break;
      default:
        Log.debug("search not empty");
        if (state.citiesSelected.isNotEmpty) {
          var newUsers = state.usersUnfiltered.where((user) {
            var isSuitableKeyword = user.name.toLowerCase().contains(searchController.text.toLowerCase());
            var citySelected = state.citiesSelected.firstWhereOrNull((element) => element.toLowerCase() == user.city.toLowerCase());
            return isSuitableKeyword && citySelected != null;
          }).toList();
          state.users = newUsers;
        } else {
          var newUsers = state.usersUnfiltered.where((user) {
            var isSuitableKeyword = user.name.toLowerCase().contains(searchController.text.toLowerCase());
            return isSuitableKeyword;
          }).toList();
          state.users = newUsers;
        }
    }
    
  }

  onChangeCheckedCity(String city,bool? isChecked) {
    state.isLoading = true;
    if (isChecked.toString() == "true") {
      state.citiesSelected.add(city);
    } else {
      state.citiesSelected.remove(city);
    }
    state.isLoading = false;
  }

  bool getValueCityCB(String city) {
    return state.citiesSelected.contains(city);
  }

  Future<void> sorting() async {
    Log.debug("sorting");
    state.isLoading = true;
    
    if (state.isAscending) {
      state.users.sort(((b, a) => a.name.toLowerCase().trimLeft().trimRight().compareTo(b.name.toLowerCase().trimLeft().trimRight())));
      state.isAscending = false;
    Log.debug("sorting isAscending");
    } else {
      state.users.sort(((a, b) => a.name.toLowerCase().trimLeft().trimRight().compareTo(b.name.toLowerCase().trimLeft().trimRight())));
      state.isAscending = true;
    Log.debug("sorting isAscending no");
    }
    state.isLoading = false;
  }

  // void addUser() {
  //   Get.toNamed(RouteName.adduser, arguments: state.cities);
  // }
}
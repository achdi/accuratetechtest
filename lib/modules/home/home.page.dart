
import 'package:accurate_achdi/modules/home/home.controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../log.utils.dart';
import '../../routename.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: SizedBox(
          height: 20,
          width: Get.width,
          child: TextField(
            onChanged: ((value) => controller.filterUser()),
            controller: controller.searchController,
            decoration: const InputDecoration(
              focusColor: Colors.white,
              fillColor: Colors.white,
              border: InputBorder.none,
              hintText: "Find your user",
              hoverColor: Colors.white,
              prefixIcon: Icon(Icons.search, color: Colors.white)
            )
          )
        ),
        actions: [
          IconButton(onPressed: () => showModalFilter(context), 
            icon: Obx(
              () => controller.state.citiesSelected.isEmpty ? 
                const Icon(Icons.filter_alt_off_outlined) : 
                const Icon(Icons.filter_alt)
            )
          ),
          IconButton(onPressed: () async => await controller.sorting(), 
            icon:  Obx(
              () => controller.state.isAscending ? 
                const Icon(Icons.keyboard_arrow_down) : 
                const Icon(Icons.keyboard_arrow_up)
            )
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () { 
          if (controller.state.isLoading) return;
          Get.toNamed(RouteName.adduser, arguments: controller.state.cities);
         },
        child: const Icon(Icons.add, color: Colors.white),
      ),
      body: Obx((() => 
         controller.state.isLoading ? 
        const Center(child: CircularProgressIndicator()) : 
        ListView.builder(
          itemCount: controller.state.users.length,
          itemBuilder: (context, index) {
            return Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: const EdgeInsets.only(left: 20,right: 20),
                    padding: const EdgeInsets.only(left: 20, right: 10, top: 20),
                    height: 150,
                    width: Get.width,
                    decoration:  BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("Name",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 16
                              )
                            ),
                            Text(controller.state.users[index].name,
                            textAlign: TextAlign.end,
                              style: const TextStyle(
                                color: Colors.black,
                                fontSize: 16
                              )
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("Email",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14
                              )
                            ),
                            Text(controller.state.users[index].email,
                              style: const TextStyle(
                                color: Color.fromARGB(255, 106, 104, 104),
                                fontSize: 14
                              )
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("Phone Number",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14
                              )
                            ),
                            Text(controller.state.users[index].phoneNumber,
                              style: const TextStyle(
                                color: Color.fromARGB(255, 106, 104, 104),
                                fontSize: 14
                              )
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("City",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14
                              )
                            ),
                            Text(controller.state.users[index].city,
                              style: const TextStyle(
                                color: Color.fromARGB(255, 106, 104, 104),
                                fontSize: 14
                              )
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(controller.state.users[index].address,
                          style: const TextStyle(
                              color: Color.fromARGB(255, 106, 104, 104),
                              fontSize: 12
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                        ),
                      ],
                    ),
                ),
                const SizedBox(
                  height: 10,
                )
              ],
            );
          }
        )
      ),
      )
    );
  }

  void showModalFilter(BuildContext param) {
    Log.debug("showModal");

    showModalBottomSheet<void>(
      context: param,
      builder: (BuildContext context) {
        return PreferredSize(
          preferredSize: const Size.fromHeight(50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Choose the cities",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16
                ),
              ),
              SizedBox(
                height: 250,
                child: Obx(() => controller.state.isLoading ? const Center(child: CircularProgressIndicator()) :  ListView.builder(
                  itemCount: controller.state.cities.length,
                  itemBuilder: (context, index) {
                    return CheckboxListTile(
                      title: Text(controller.state.cities[index].name),
                      value: controller.state.citiesSelected.contains(controller.state.cities[index].name),
                      onChanged: (bool? value) => controller.onChangeCheckedCity(controller.state.cities[index].name, value)
                    );
                  }
                )
                )
              ),
              MaterialButton(
                minWidth: Get.width,
                color: Colors.blue,
                textColor: Colors.white,
                onPressed: () => {
                  Get.back(),
                  controller.filterUser(),
                },
                child: const Text("Done"),
              )
            ],
          )
        );
      },
    );
  }

}
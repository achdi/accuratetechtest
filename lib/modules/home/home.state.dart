
import 'package:accurate_achdi/model/city.model.dart';
import 'package:accurate_achdi/model/user.model.dart';
import 'package:get/state_manager.dart';

class HomeState {
  final _users = RxList<UserModel>();
  set users(value) => _users.value = value;
  List<UserModel> get users => _users;

  final _usersUnfiltered = RxList<UserModel>();
  set usersUnfiltered(value) => _usersUnfiltered.value = value;
  List<UserModel> get usersUnfiltered => _usersUnfiltered;

  final _isLoading = RxBool(true);
  set isLoading(value) => _isLoading.value = value;
  get isLoading => _isLoading.value;

  final _cities = RxList<CityModel>();
  set cities(value) => _cities.value = value;
  List<CityModel> get cities => _cities;

  final _citiesSelected = RxList<String>();
  set citiesSelected(value) => _citiesSelected.value = value;
  List<String> get citiesSelected => _citiesSelected;

  final _isAscending = RxBool(true);
  set isAscending(value) => _isAscending.value = value;
  get isAscending => _isAscending.value;
}


import 'package:accurate_achdi/modules/adduser/adduser.controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';

import '../../log.utils.dart';

class AddUserPage extends GetView<AddUserController> {

  const AddUserPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add User"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            FormBuilder(
              key: controller.formKey,
              autovalidateMode: AutovalidateMode.disabled,
              child: _formFields(context),
            ),
            const SizedBox(
              height: 30,
            ),
            MaterialButton(
              minWidth: Get.width,
              height: 60,
              color: Colors.blue,
              textColor: Colors.white,
              onPressed: () {
                Log.debug("MaterialButton press");
                if (controller.state.isLoading) return;
                if (controller.formKey.currentState!.validate()) {
                  
                  Log.debug("form isvalid");
                  // If the form is valid, display a snackbar. In the real world,
                  // you'd often call a server or save the information in a database.
                  // ScaffoldMessenger.of(context).showSnackBar(
                  //   const SnackBar(content: Text('Processing Data')),
                  // )
                  controller.saveUser();
                }
              },
              child: Obx(() => controller.state.isLoading ? const CircularProgressIndicator() : const Text("Done"))
            )
          ],
        ),
      )
    );
  }

  Widget _formFields(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 27,
        ),
        FormBuilderTextField(
          controller: controller.nameController,
          name: 'name',
          decoration: const InputDecoration(
              hintText: "input name",
              labelText: "Name",
          ),
          keyboardType: TextInputType.name,
          validator: FormBuilderValidators.compose([
            FormBuilderValidators.required(),
            FormBuilderValidators.minLength(5),
            FormBuilderValidators.maxLength(250),
          ]),
        ),
        const SizedBox(
          height: 10,
        ),
        FormBuilderTextField(
          controller: controller.emailController,
          name: 'email',
          decoration: const InputDecoration(
              hintText: "input email",
              labelText: "Email"
          ),
          keyboardType: TextInputType.emailAddress,
          validator: FormBuilderValidators.compose([
            FormBuilderValidators.required(),
            FormBuilderValidators.email(),
            FormBuilderValidators.minLength(5),
            FormBuilderValidators.maxLength(250),
          ]),
        ),
        const SizedBox(
          height: 10,
        ),
        FormBuilderTextField(
          controller: controller.phoneController,
          name: 'phone',
          decoration: const InputDecoration(
              hintText: "input phone number",
              labelText: "Phone Number"
          ),
          keyboardType: TextInputType.phone,
          validator: FormBuilderValidators.compose([
            FormBuilderValidators.required(),
            FormBuilderValidators.numeric(),
            FormBuilderValidators.minLength(5),
            FormBuilderValidators.maxLength(250),
          ]),
        ),

        const SizedBox(
          height: 10,
        ),
        GestureDetector(
          onTap: (() => showModalCity(context)),
          child: FormBuilderTextField(
            showCursor: false,
            controller: controller.cityController,
            readOnly: true,
            name: 'city',
            decoration: InputDecoration(
              suffixIcon: IconButton(
                icon: const Icon(Icons.keyboard_arrow_down), 
                onPressed: (() => showModalCity(context))
              ),
              hintText: "City",
            ),
            keyboardType: TextInputType.text,
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(),
              FormBuilderValidators.minLength(5),
              FormBuilderValidators.maxLength(250),
            ]),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        FormBuilderTextField(
          controller: controller.addressController,
          name: 'address',
          decoration: const InputDecoration(
              hintText: "input address",
              labelText: "Address"
          ),
          keyboardType: TextInputType.name,
          validator: FormBuilderValidators.compose([
            FormBuilderValidators.required(),
            FormBuilderValidators.minLength(5),
            FormBuilderValidators.maxLength(250),
          ]),
        ),
      ],
    );
  }

  void showModalCity(BuildContext param) {
    showModalBottomSheet<void>(
      context: param,
      builder: (BuildContext context) {
        return PreferredSize(
          preferredSize: const Size.fromHeight(50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Choose the cities",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16
                ),
              ),
              const SizedBox(height: 30),
              SizedBox(
                height: 250,
                child: Obx(() => controller.state.isLoading ? const Center(child: CircularProgressIndicator()) :  ListView.builder(
                  itemCount: controller.cities.length,
                  itemBuilder: (context, index) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () => controller.chooseCity(controller.cities[index].name),
                          child: Container(
                            width: Get.width,
                            padding: const EdgeInsets.all(10),
                            child: Text(controller.cities[index].name),
                          )
                        ),
                        const Divider()
                      ],
                    );
                  }
                )
                )
              ),
            ],
          )
        );
      },
    );
  }
}
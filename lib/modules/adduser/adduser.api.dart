import 'package:accurate_achdi/model/user.model.dart';
import 'package:dio/dio.dart';

import '../../log.utils.dart';
import '../../network/dio_client.dart';
import '../../network/endpoint.dart';

class AddUserAPI {
  Future<UserModel> getUser(param) async {
    try {
      var usersJSON = await DioClient().post(Endpoint.user, param);
      var users = UserModel.fromJson(usersJSON);

      return users;
    } on DioError catch (e) {
      if (e.response != null) {
        Log.debug('Dio error!');
        Log.debug('STATUS: ${e.response?.statusCode}');
        Log.debug('DATA: ${e.response?.data}');
        Log.debug('HEADERS: ${e.response?.headers}');
      } else {
        // Error due to setting up or sending the request
        Log.debug('Error sending request!');
        Log.debug(e.message);
      }
      return Future.error(e);
    } catch (e) {
      Log.debug("error other");
      Log.debug(e);
      return Future.error(e);
    }
  }
}
import 'package:accurate_achdi/modules/adduser/adduser.api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../log.utils.dart';
import '../../model/city.model.dart';
import 'adduser.state.dart';

class AddUserController extends GetxController {
  final formKey = GlobalKey<FormBuilderState>();
  final state = AddUserState();

  final List<CityModel> cities = Get.arguments;
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController cityController = TextEditingController();

  Future<void> saveUser() async {
    Log.debug("saveUser");
    state.isLoading = true;
    var newUser = {
      "name": nameController.text,
      "email": emailController.text,
      "phoneNumber": phoneController.text,
      "city": cityController.text,
      "address": addressController.text,
    };
    AddUserAPI().getUser(newUser).then((value) {
      state.isLoading = false;
      Fluttertoast.showToast(
        msg: "Data user is success saved",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP_LEFT,
        timeInSecForIosWeb: 1,
        backgroundColor: const Color.fromARGB(128, 13, 12, 12),
        textColor: Colors.white,
        fontSize: 16.0
      );
      Get.back();
    });
  }

  Future<void> chooseCity(String city) async {
    cityController.text = city;
    Get.back();
  }
}
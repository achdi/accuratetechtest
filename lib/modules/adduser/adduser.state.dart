
import 'package:get/get.dart';

class AddUserState {
  final _isLoading = RxBool(false);
  set isLoading(value) => _isLoading.value = value;
  get isLoading => _isLoading.value;
}
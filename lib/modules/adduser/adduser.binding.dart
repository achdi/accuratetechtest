
import 'package:accurate_achdi/modules/adduser/adduser.controller.dart';
import 'package:get/get.dart';

class AddUserBinding extends Bindings {
   @override
  void dependencies() {
    Get.lazyPut<AddUserController>(() => AddUserController());
  }
}
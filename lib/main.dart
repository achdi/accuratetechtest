import 'package:accurate_achdi/modules/adduser/adduser.binding.dart';
import 'package:accurate_achdi/modules/adduser/adduser.page.dart';
import 'package:accurate_achdi/modules/home/home.binding.dart';
import 'package:accurate_achdi/modules/home/home.page.dart';
import 'package:accurate_achdi/routename.dart';
import 'package:flutter/material.dart';
import 'package:form_builder_validators/localization/l10n.dart';
import 'package:get/get.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      localizationsDelegates: const [
        FormBuilderLocalizations.delegate,
      ],
      supportedLocales: FormBuilderLocalizations.delegate.supportedLocales,
      initialRoute: RouteName.home,
      getPages: [
        GetPage(
          name: RouteName.home, 
          binding: HomeBinding(),
          page: () => const HomePage(),
        ),
        GetPage(
          name: RouteName.adduser, 
          binding: AddUserBinding(),
          page: () => const AddUserPage(),
        ),
      ],
    );
  }
}
